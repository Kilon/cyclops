/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */

#include <string.h>
#include <stdio.h>

#include "ephestos.h"

struct aci_imported_functions *aci_if;

int hec_morph_init(Morph *morph)
{
	morph->x =0;
	morph->y = 0;
	morph->width = 0;
	morph->height = 0;
	morph->texture =NULL;
	morph->type = "";
	morph->id = 0;
	morph->operator_name = "";
	morph->text = "";
	morph->texture = NULL;
	morph->but = NULL;
	morph->next = NULL;
	morph->previous = NULL;
	morph->world = aci_if->eph_hecate_get_world();

	return 0;
};

TextureMorph *hec_texture_find_named(char *name)
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	TextureMorph *current_texture; 
	current_texture = world->first_texture;
	while(current_texture)
	{
		if(strcmp(current_texture->name,name)==0)
		{
			return current_texture;
		}
		current_texture = current_texture->next;
		if(current_texture)
		{
			world->last_texture = current_texture;
		}
	}
	return NULL;
};

bool hec_morph_is_duplicate(Morph *morph_a, Morph *morph_b)
{
	bool is_duplicate;
	is_duplicate =  morph_a->x == morph_b->x && 
		morph_a->y == morph_b->y &&
		morph_a->width == morph_b->width &&
		morph_a->height == morph_b->height &&
		morph_a->texture->name == morph_b->texture->name &&
		morph_a->type == morph_b->type &&
		morph_a->id == morph_b->id &&
		morph_a->operator_name == morph_b->operator_name &&
		morph_a->text == morph_b->text;
		return is_duplicate;
}

int hec_catalogue_delete()
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	Morph *current_morph=NULL;
	bool end_of_catalogue = false;
	while(!end_of_catalogue)
	{
		if(!world->first_morph)
		{
			end_of_catalogue = true;
		}
		else
		{
			if(!current_morph)
			{
				current_morph = world->first_morph;
			}
			else
			{	
				if(current_morph->next)
				{
					current_morph = current_morph->next;
					aci_if->eph_free(current_morph->previous);
				}
				else
				{
					aci_if->eph_free(current_morph);
					world->first_morph = NULL;
					world->last_morph = NULL;
					end_of_catalogue = true;
				}
			}
		}
	}
	return HEC_NO_ERROR;
}

int sizeof_catalogue()
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	int count=0;
	Morph *current_morph=NULL;
	current_morph = world->first_morph;
	while(current_morph)
	{
		count++;
		current_morph = current_morph->next;
	}
	return count;	
} 

/* dummy function to test live reloading and coding of components */
static void main_loop()
{
		printf("main loop has executed. \n");
};

/* another dummy function to test that a component is properly loaded */
static int test_fun(aci_arg *args)
{
	printf("executing test_fun with arguments %s and %f \n",args[0].string_value, args[1].double_value);
	return 0;
};


/* create the hec_morph_catalog array and keep track of how many elements are appended */
static int hec_morph_catalog_initialise(aci_arg *args)
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	world->should_refresh = false;
	hec_catalogue_delete();
	return 0;

};

/* sets the world cordinates and bounds , args are (int x, int y, int width, int height)*/
static int hec_world_set(aci_arg *args)
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	world -> x = args[0].long_value;
	world -> y = args[1].long_value;
	world -> width = args[2].long_value;
	world -> height = args[3].long_value;
	world -> py_region_x = args[4].long_value;
	world -> py_region_y = args[5].long_value;
	world -> py_region_width = args[6].long_value;
	world -> py_region_height = args[7].long_value;
	world -> py_mouse_x = args[8].long_value;
	world -> py_mouse_y = args[9].long_value;
	world -> is_hidden = args[10].long_value;

	return HEC_NO_ERROR;	
};

/* sets the world cordinates and bounds , args are (int x, int y, int width, int height)*/
static int hec_world_translate(aci_arg *args)
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	aci_if->bl_UI_block_translate(world->block,args[0].long_value,args[1].long_value);
	return HEC_NO_ERROR;	
};

/* append to the morph catalogue and increment the counter */
static int hec_morph_catalog_append(aci_arg *args)
{
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	Morph *current_morph = NULL;
	TextureMorph *current_texture = NULL;

	bool end_find = false;
	bool is_duplicate = false;

	Morph *morph_to_be_appended=(Morph*)aci_if->eph_malloc(sizeof(Morph));
	if(morph_to_be_appended == NULL)
	{
		printf("Hecate : memory allocation failed for the creation of new morph !!! \n");
			return HEC_ERROR_MORPH_MALLOC_FAILED;
	}
	
	TextureMorph *texture_to_be_appended; 
	texture_to_be_appended = hec_texture_find_named(args[4].string_value); 
	if(!texture_to_be_appended)
	{
		texture_to_be_appended = (TextureMorph*)aci_if->eph_malloc(sizeof(TextureMorph));
		if(texture_to_be_appended == NULL)
		{
			printf("Hecate : memory allocation failed for the creation of new texture !!! \n");
			return HEC_ERROR_TEXTURE_MALLOC_FAILED;
		}
		texture_to_be_appended->image_buffer = NULL;
		texture_to_be_appended->image_buffer_backup = NULL;
		texture_to_be_appended->name = args[4].string_value;
		texture_to_be_appended->next = NULL;
		texture_to_be_appended->previous = NULL;
		if(!world->first_texture)
		{
			world->first_texture = texture_to_be_appended;
		}
		world->last_texture = texture_to_be_appended;
		current_texture = world->first_texture;
		while (current_texture && current_texture != texture_to_be_appended)
		{
			if(current_texture->next)
			{
				current_texture = current_texture->next;
			}
			else
			{
				current_texture->next = texture_to_be_appended;
				texture_to_be_appended->previous = current_texture;
				current_texture = NULL;
			}
		}

	}
	
	morph_to_be_appended->x =args[0].long_value;
	morph_to_be_appended->y = args[1].long_value;
	morph_to_be_appended->width = args[2].long_value;
	morph_to_be_appended->height = args[3].long_value;
	morph_to_be_appended->texture = texture_to_be_appended;
	morph_to_be_appended->type = args[5].string_value;
	morph_to_be_appended->id = args[6].long_value;
	morph_to_be_appended->operator_name = args[7].string_value;
	morph_to_be_appended->text = args[8].string_value;

	current_morph = world->first_morph;
	
	while(current_morph)
	{
		is_duplicate = hec_morph_is_duplicate(current_morph,morph_to_be_appended);
		if(is_duplicate)
		{
			aci_if->eph_free(morph_to_be_appended);
			return HEC_NO_ERROR;
		}
		current_morph = current_morph->next;
	}
	morph_to_be_appended->world = world;
	if(world->last_morph)
	{
		morph_to_be_appended->previous =world->last_morph;
		world->last_morph->next = morph_to_be_appended;
	}
	else
	{
		morph_to_be_appended->previous =NULL;
	}
	morph_to_be_appended->next = NULL;
	world->last_morph = morph_to_be_appended;
	if(!world->first_morph)
	{
		world->first_morph = morph_to_be_appended;
	}

	return HEC_NO_ERROR;
};

/* mark the end of the catalogue so the iteration knows where to stop */
static int hec_morph_catalog_end(aci_arg *args)
{	
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();

	if(world->region && world->region->visible==1 )
	{
		//aci_if->ED_region_tag_redraw(world->region);
		world->region->do_draw = 24;
		//world->region = NULL;
	}
	world->should_refresh = true;
	
	return HEC_NO_ERROR;
};

static int hec_close_right_click_menu(aci_arg *args)
{
	/* wmWindow *win = CTX_wm_window(C);
	UI_popup_block_close(C, win, args[0].block_pointer); */
}

/* locates an image file , usually with .PNG extension inside the 
[blender_folder]/[version]/datafiles/ephestos/ and generates its absolute path */
static int hec_find_image_path(char *filename,char *image_filepath)
{
	#if defined(WIN32)
		char *subfolder = "ephestos\\hal\\icons\\";
	#else
		char *subfolder = "ephestos/hal/icons/";
	#endif
	const char *folder = aci_if->bl_BKE_appdir_folder_id(BLENDER_SYSTEM_DATAFILES, subfolder);
	snprintf(image_filepath, 256, "%s%s", folder, filename);
	return HEC_NO_ERROR;
};

 /* creates a button using the filename of an image file as icon ,
 an operator that is triggered when button is clicked, position x,y
 width, height and text for tooltip */
 static Morph *hec_create_morph(uiBlock *block, Morph *morph)
 {
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();

	TextureMorph *found_texture;
	TextureMorph *current_texture;
	TextureMorph *previous_texture;
	char image_filepath[256];
	if(strcmp(morph->type,"label")!=0)
	{	
		
		found_texture = hec_texture_find_named(morph->texture->name);
		//found_texture = NULL;
		
		/* problem with Blender erasing texture on refresh */
		if(found_texture && found_texture->image_buffer_backup )
		{
			found_texture->image_buffer = aci_if->bl_IMB_dupImBuf(morph->texture->image_buffer_backup);
			morph->texture->image_buffer = found_texture->image_buffer;
		}
		else
		{
			hec_find_image_path(morph->texture->name,image_filepath);
			morph->texture->image_buffer = aci_if->bl_IMB_loadiffname(image_filepath, IB_rect, NULL);
			morph->texture->image_buffer_backup = aci_if->bl_IMB_dupImBuf(morph->texture->image_buffer);
	
			if(!world->first_texture)
			{
				
				world->first_texture = morph->texture;
				world->last_texture = morph->texture;
			}
			else
			{
				current_texture = world->first_texture;
			}
			
			while(current_texture->next)
			{
				previous_texture = current_texture;
				current_texture=current_texture->next;
			}
			morph->texture->previous = previous_texture;
			world->last_texture = morph->texture;
		}
		
		
	}	
	if(strcmp(morph->type,"operator")==0)
	{
		morph->but = aci_if->bl_uiDefIconButO(block, UI_BTYPE_BUT_TOGGLE/*UI_BTYPE_IMAGE*/, morph->operator_name, WM_OP_INVOKE_DEFAULT, ICON_NONE, morph->x, morph->y, (short)morph->width, (short)morph->height,"");
		morph->but->poin = (char *)morph->texture->image_buffer;
		morph->but->tip=NULL;
	}
	if(strcmp(morph->type,"default")==0)
	{
		morph->but = aci_if->bl_uiDefBut(block,UI_BTYPE_IMAGE, 0, "", morph->x, morph->y, (short)morph->width, (short)morph->height, morph->texture->image_buffer,0,0,0,0, "");
		morph->but->tip=NULL;
		//morph->but->flag = UI_BUT_DRAG_MULTI;
		//morph->but->poin = (char *)morph->texture->image_buffer;
	}
	if(strcmp(morph->type,"label")==0)
	{
		morph->but = aci_if->bl_uiDefBut(block, UI_BTYPE_LABEL, 0, morph->text, morph->x, morph->y, (short)morph->width, (short)morph->height, NULL,0.0,0.0,0,0, "");
		aci_if->bl_UI_but_flag_enable(morph->but, 1);
		morph->but->tip=NULL;
	}
	
	return morph;
 };

static int hec_keep_inside_region()
{
	WorldMorph *hec_world;
	hec_world = aci_if->eph_hecate_get_world();
	char log[200];
	if(hec_world->region->winrct.xmax !=0)
	{
		sprintf(log,"winrct : %d , %d, %d, %d  blockrct: %d, %d, %d, %d",(int)hec_world->region->winrct.xmin,(int)hec_world->region->winrct.ymin,(int)hec_world->region->winrct.xmax,(int)hec_world->region->winrct.ymax, (int)hec_world->block->rect.xmin,(int)hec_world->block->rect.ymin,(int)hec_world->block->rect.xmax,(int)hec_world->block->rect.ymax);

		//aci_if->bl_uiDefBut(hec_world->block, UI_BTYPE_LABEL, 0, log, 10, 340,340, 10, NULL,0.0,0.0,0,0, "");
		//Sleep(1000);
		
		if((hec_world->x -12 ) != hec_world->region->winrct.xmin)
		{
			hec_world->offset_x = hec_world->region->winrct.xmin -  (hec_world->x -12);
		}
		if((hec_world->y  - 12)!= hec_world->region->winrct.ymin)
		{
			hec_world->offset_y = hec_world->region->winrct.ymin- (hec_world->y - 12) ;
		}
		
	}
	else
	{
		hec_world->offset_x = 0;
		hec_world->offset_y = 0;
	}
		
	aci_if->bl_UI_block_translate(hec_world->block,-hec_world->offset_x,-hec_world->offset_y);
	
	return HEC_NO_ERROR;
};
 /* iterates through the scruct containing button info and creates the buttons */
static int hec_buttons_init(aci_arg *args)
 {
	WorldMorph *world = NULL;
	world = aci_if->eph_hecate_get_world();
	//Sleep(10);
	uiBlock *block;
	block=args[0].block_pointer;
	world->region = args[1].region;
	aci_if->bl_UI_block_bounds_set_popup(block, 0, (const int[2]){0, 0});
	if(world->first_morph && world->should_refresh && world->is_hidden == 0)
	{
		Morph *current_morph;
		current_morph = world->first_morph;
		while(current_morph)
		{
			hec_create_morph(block,current_morph);
			current_morph = current_morph->next;
		}
		hec_keep_inside_region();
	}
	
	return HEC_NO_ERROR;
 };

/* the list of the internal functions to be exported. These functions can then be 
used internally by Blender but also externally through Ephestos Python API using 
the module ephpy.apy.ci by the request method */
aci_exported_functions_registry aci_reg[] = {
	{.registered_function = hec_buttons_init, .name="hec_buttons_init",.args_count=2, .args_types={"block_pointer","region"}},
	{.registered_function = test_fun,.name="test_fun",.args_count=2,.args_types={"string","double"}},{.registered_function = hec_morph_catalog_initialise,.name="hec_morph_catalog_initialise",.args_count=0,.args_types=NULL},
	{.registered_function = hec_morph_catalog_append,.name="hec_morph_catalog_append",.args_count=9,.args_types={"long","long","long","long","string","string","long","string","string"}},
	{.registered_function = hec_morph_catalog_end,.name="hec_morph_catalog_end",.args_count=0,.args_types=NULL},
	{.registered_function = hec_world_set,.name="hec_world_set",.args_count=11,.args_types={"long","long","long","long","long","long","long","long","long","long","long"}},
	{.registered_function = hec_world_translate,.name="hec_world_translate",.args_count=2,.args_types={"long","long"}}
	};

/* it imports pointers to blender function without these pointers we cannnot 
access blender's internal functionality */
EXPORT_DLL void aci_import_functions_table(struct aci_imported_functions *blender_functions_struct)
{
	 aci_if = blender_functions_struct;
};

/* this big boy is the request function that allows access to execute component's
internal fucntions from inside Blender source or Blender addons. This function is
 needed because:
	a) It allows us to avoid to have to wrap/ manually expose each function to 
	python which would need to change Blender source and rebuild it

	b) it allows us to implement functionality without having to change Blender source thus it minimises the possibility of conflicts and incompatibilities
	 while it gives us both modularity and C performance/speed 

	c) it allows to closely control the execution of those functions, when , where and if we permit it , to keep top performance and minimize possible side affects

	d) it allows to easily keep some functions inaccesible from outside the component
	it avoid possible side effects
	
*/
EXPORT_DLL int aci_request(char *function_name, int args_count, struct aci_arg *aci_args, char error_message[80])
{
	int aci_reg_size = (int)(sizeof(aci_reg)/sizeof(aci_exported_functions_registry));
	for(int x=0; x < aci_reg_size;x++)
	{
		if(strcmp(function_name,aci_reg[x].name )==0)
		{
			
			if(args_count==aci_reg[x].args_count && aci_reg[x].args_count != 0)
			{
				for (int y=0; y < aci_reg[x].args_count;y++)
				{
					if(strcmp(aci_args[y].type, aci_reg[x].args_types[y])!=0)
					{
						sprintf(error_message,"aci: %s function's arg %d should be of type %s \n",function_name,y+1,aci_reg[x].args_types[y]);
						return -1;
					}
					
				}
			}
			else if(args_count!=aci_reg[x].args_count)
			{
				sprintf(error_message,"aci: %s function found but you should have used %d args!!!\n",function_name, (int)(sizeof(aci_reg[x].args_types)));
				return -1;
			}
			if(aci_reg[x].registered_function(aci_args)==0)
			{
				sprintf(error_message,"done");
				return 0;
			}
			else
			{
				sprintf(error_message,"aci: error (-1) returned executing %s",function_name);
			}
			
		}
	}
	sprintf(error_message,"aci: cannot find the %s function\n",function_name);
	return -1;

};



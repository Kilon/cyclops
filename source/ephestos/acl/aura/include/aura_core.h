#ifndef __EPH__AURA_CORE_H__
#define __EPH__AURA_CORE_H__

#include "BLI_dynlib.h"

#ifdef _WIN32
#define EXPORT_DLL __declspec(dllexport)
#define IMPORT_DLL __cdecl
#else
#define EXPORT_DLL //__attribute__((visibility("default")))
#define IMPORT_DLL
#endif

typedef struct AComponent AComponent;
typedef struct AProtoComponent AProtoComponent;
typedef struct aci_arg aci_arg;
typedef struct aci_exported_functions_registry aci_exported_functions_registry;
typedef struct aci_registry aci_registry;
typedef struct aci_imported_functions;

struct AComponent
{
	char *name;
	AComponent *parent;
	AComponent *children;
	char *type;
	char *inputs;
	char *outputs;
	char *functions;
	char *inherited_functions;
	AComponent *(*request)(aci_arg *args);
	AComponent *(*r)(aci_arg *args);
};



/* this is the registry that keeps track of the internal functions in a component
that it exports. For possible types look at aci_arg */
struct aci_exported_functions_registry{
	int (*registered_function)(struct aci_arg *args);
	char *name;
	int args_count;
	char args_types[20][20];
};

/* this is a simple registry entry field for components that are loaded,
this way we keep track of the component loaded so a component won't have 
to be loaded more than one time */
struct aci_registry{
	DynamicLibrary *component;
	char *name;
};

/* perform an aura component request to execute a function from a component */
EXPORT_DLL int aci_request(char *function_name, int args_count,struct aci_arg *aci_args, char error_message[80]);
typedef int (*aci_request_def)(char *function_name, int args_count, struct aci_arg *args,char error_message[80] );
aci_request_def aci_request_if;

/* import pointers to blender functions inside a component without 
this pointer the component will be unable to access internal blender
functions */
EXPORT_DLL void aci_import_functions_table(struct aci_imported_functions *blender_functions_struct);
typedef void (*aci_import_functions_table_def)(struct aci_imported_functions *blender_functions_struct);
aci_import_functions_table_def export_functions_table;


/* functions to load and unload components  path is relative to folder
[blender_folder]/2.80/datafiles/ephestos/acl */
DynamicLibrary* aci_load(char *component_name, char *relative_path);
DynamicLibrary* aci_unload(char *component_name);


#endif

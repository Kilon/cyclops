.. ephestos documentation master file, created by
   sphinx-quickstart on Sun Mar 24 02:32:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ephestos's documentation!
====================================
Ephestos is a 3d graphics creation application based on Blender source code (and distrubuted under the same license) focusing on boosting artist creativity with introducing two subprojects **Hecate** and **Aura**.
**Hecate's** goal is to provide a direct and very productive UI for simple and complex tasks, without the need for diving inside menus and submenus and trying to minimise mouse movement while the same time avoiding the need for keyboard shortcuts. While **Aura's** goal is to improve organisation extending far beyone the typical abilities of an asset manager. This documentation contains two manuals. One for users that want to get going right away on how to use Ephestos. One for users that are also addon developers and like to take full advantage of Ephestos' capabities by accessing it's Ephestos Python Api (ephpy)

.. toctree::
   :maxdepth: 6
   :caption: Contents:

   user/user
   
   dev/dev
   



Indices and tables
==================


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
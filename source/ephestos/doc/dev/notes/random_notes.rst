
#############
Random Notes
#############

Here you will find documentation that is not organised. Not for general usage but if you look for extra information this a good place to start.

****************************
Blender Source Documentation
****************************

Here you will find documentation focusing on Blender source code

RegionView3D
=============

.. c:struct:: RegionView3D

    Viewport camera can be obtained via region, code
  
    .. code-block:: c 
    
        ARegion *region = CTX_wm_region(C);
        RegionView3D *rv3d = region->regiondata;

    its definition can be found at **blender/source/blender/makesdna/DNA_view3d_types.h**

    .. c:var:: float viewquat[4]
      
        **rotation** of viewport

    .. c:var:: float dist

        **distance** of viewport from point of focus

    .. c:var:: float ofs[3]

        **location** of viewport? in world space


ED_object_rotation_from_view(C, rot, view_align_axis);



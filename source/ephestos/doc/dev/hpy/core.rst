
##########
Hecate API
##########

.. py:module:: ephpy.hpy



To import Hecata Python API to your addon you will have to issue the following command

.. code-block:: python

    import eph.hpy

This will import all the relevant classes you will need together with the individual modules. 

The structure of the Hecate API modules is as following (click on the lower nodes to navigate to individual modules)

.. graphviz::
    :caption: Ephestos Python API
    :name: ephpy-structure
    
    digraph module {
        bgcolor = "bisque";
        node [shape=folder];
        ephpy [ pos="0,3!", label = "ephpy&#92;n[Ephestos Python API]", style="filled", color="seagreen"];
        hpy [pos="0,2!",label = "hpy&#92;n[Hecate API]",style="filled",color="orchid"];
        core [pos="0,0!",label = "core", style=filled,color=".7 .3 1.0",URL="#core-py"];
        texture [pos="1,0!",label = "texture", style=filled,color=".7 .3 1.0", URL="#module-ephpy.hpy.texture"];
        event [pos="2,0!",label = "event", style=filled,color=".7 .3 1.0", URL="#module-ephpy.hpy.event"];
        layout [pos="3,0!",label = "layout_engine", style=filled,color=".7 .3 1.0", URL="#module-ephpy.hpy.layout_engine"];
        pylivecoding [pos="4,2",label = "pylivecoding&#92;n[live code reload]", style=filled,color="Turquoise", URL="#module-ephpy.pylivecoding"];
        ephpy -> hpy;
        hpy -> core;
        hpy -> texture;
        hpy -> event;
        hpy -> layout;
        ephpy -> pylivecoding;
        pylivecoding -> core [style=dotted,color="Turquoise"];
        pylivecoding -> event [style=dotted,color="Turquoise"];
        pylivecoding -> texture [style=dotted,color="Turquoise"];
        pylivecoding -> layout [style=dotted,color="Turquoise"];

        }

*******
core.py
*******

.. py:module: ephpy.hpy.core
    :synopsis: The core module for Hecate

Core module for Hecate, main morph definitions


Morph
=====

.. autoclass:: ephpy.hpy.core.Morph


World
=====

.. autoclass:: ephpy.hpy.core.World

ButtonMorph
===========

.. autoclass:: ephpy.hpy.core.ButtonMorph

SwitchButtonMorph
=================

.. autoclass:: ephpy.hpy.core.SwitchButtonMorph

KnobMorph
=========

.. autoclass:: ephpy.hpy.core.KnobMorph

TooltipMorph
============

.. autoclass:: ephpy.hpy.core.TooltipMorph

LineBarMorph
============

.. autoclass:: ephpy.hpy.core.LineBarMorph

**********
texture.py
**********

.. py:module:: ephpy.hpy.texture

Texture
=======

.. autoclass:: ephpy.hpy.texture.Texture

********
event.py
********

.. py:module:: ephpy.hpy.event

EventProcessor
==============

.. autoclass:: ephpy.hpy.event.EventProcessor


TooltipEventProcessor
=====================

.. autoclass:: ephpy.hpy.event.TooltipEventProcessor

****************
layout_engine.py
****************

.. py:module:: ephpy.hpy.layout_engine

LayoutEngine
============

.. autoclass:: ephpy.hpy.layout_engine.LayoutEngine
    

   
    
    

     
       
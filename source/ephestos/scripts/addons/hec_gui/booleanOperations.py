"""/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */"""

import bpy
from . import pylivecoding as livecoding
import pdb

class GenericBooleanOperations(livecoding.LiveObject):
    # activeObject = bpy.context.active_object
    target_objects = None
    instances = []

    def find_axis_range(bounding_box, location):

        axis_range = []
        bounding_box_list = __class__.convert_bounding_box_to_python_list()
        values = []

        #find minimum and maximum for each corner (8)
        for axis in range(0, 3):

            for point in bounding_box_list:
                values.append(point[axis] + location[axis])

            axis_range.append([min(values), max(values)])
            values = []

        return axis_range

    def convert_bounding_box_to_python_list(bounding_box):
        bounding_box_list = []

        # convert  bound_box to python list
        for corner in bounding_box:
            element = []
            for axis_point in corner:
                element.append(axis_point)
            bounding_box_list.append(element)

        return  bounding_box_list

    def compare_axis_range(axis_range1, axis_range2):
        ar1 = axis_range1
        ar2 = axis_range2
        is_in_range = [False, False, False]
        for axis in range(0, 3):
            if (ar2[axis][0] < ar1[axis][1] and ar2[axis][1] > ar1[axis][0]):
                is_in_range[axis] = True
        if is_in_range[0] and is_in_range[1] and is_in_range[2]:
            return True
        return False

    def perform_boolean_operation(operation):

        duplication = False
        parenting = False
        scene = bpy.context.scene
        selected_objects = bpy.context.selected_objects
        scene_objects = bpy.context.scene.objects
        GenericBooleanOperations.active_object = bpy.context.active_object
        print(bpy.context.selected_objects)

        bound_box = bpy.context.active_object.bound_box
        active_object_location = bpy.context.active_object.location
        selected_object_axis_range = GenericBooleanOperations.find_axis_range(bound_box, active_object_location)
        print(bpy.context.active_object.location)

        meshes_locations = []
        if bound_box == bound_box:
            print("TRUE")
        if len(selected_objects) == 1:
            for check_object in scene_objects:
                if check_object.type == 'MESH' and not check_object.select:
                    check_object_bound_box = check_object.bound_box
                    check_object_location = check_object.location
                    check_object_axis_range = GenericBooleanOperations.find_axis_range(check_object_bound_box, check_object_location)

                    if GenericBooleanOperations.compare_axis_range(selected_object_axis_range, check_object_axis_range):
                        check_object.select = True
                        print(selected_objects)
                        GenericBooleanOperations.active_object = check_object
                        bpy.context.scene.objects.active = GenericBooleanOperations.active_object


                        break

        for check_object in bpy.context.selected_objects:
            if check_object != GenericBooleanOperations.active_object:
                bpy.ops.object.modifier_add(type='BOOLEAN')
                for mod in GenericBooleanOperations.active_object.modifiers:

                    if mod.type == 'BOOLEAN' and mod.object is None:

                        if duplication:
                            bool_object = check_object.copy()
                            bool_object.data = check_object.data.copy()
                            bool_object.location = check_object.location
                            bool_object.select = False
                            scene.objects.link(bool_object)

                        elif not duplication:
                            bool_object = check_object

                        if parenting:
                            bool_object.parent = GenericBooleanOperations.active_object

                        mod.operation = operation
                        mod.object = bool_object
                        mod.show_expanded = False

                        bool_object.hide_render = True
                        bool_object.draw_type = 'BOUNDS'
                        cv = check_object.cycles_visibility
                        cv.camera = False
                        cv.diffuse = False
                        cv.glossy = False
                        cv.scatter = False
                        cv.shadow = False
                        cv.transmission = False


class UnionButtonAction(GenericBooleanOperations):
    instances = []
    def on_left_click(morph):
        GenericBooleanOperations.perform_boolean_operation('UNION')


class DifferenceButtonAction(GenericBooleanOperations):
    instances = []
    def on_left_click(morph):
        GenericBooleanOperations.perform_boolean_operation('DIFFERENCE')


class IntersectButtonAction(GenericBooleanOperations):
    instances = []
    def on_left_click(morph):
        GenericBooleanOperations.perform_boolean_operation('INTERSECT')

"""/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */"""

bl_info={
"name": "Hecate GUI",
"description": "Hecate GUI v 0.1",
"author": "Kilon ",
"version": (0, 1),
"blender": (2, 80, 0),
"location": "View3D > Tools Panel /Properties panel",
"wiki_url": "https://kilon.gitlab.io/index.html",
"tracker_url": "https://gitlab.com/Kilon/ephestos/issues",
"category": "User Interface"}

if "bpy" in locals():
    import importlib
    if 'hec_gui_startup' in locals():
        modules = (hec_gui_startup,)
        for m in modules:
            importlib.reload(m)
else:
    from . import hec_gui_startup
import bpy, sys, traceback, ephpy, pdb, time,cProfile,pstats,io,datetime,logging,os

from bpy.types import Operator, AddonPreferences
from bpy.props import StringProperty, IntProperty, BoolProperty, FloatProperty



class EPHESTOS_OT_hec_gui(bpy.types.Operator):
    """ This operator is responsible for triggering and preparing the Hecate GUI. It keeps state that is used by other modules and makes sure only one instance of the class exists at time to avoid potential conflicts with multiple instances trying to handle the same event. pylivecoding is also used on mouseclick to reload the modules if dev_mode is true,
at the same time the debugger will be triggered if an error occurs """

    #Central Modal Operator
    bl_idname = "object.hec_gui"
    bl_label = 'Hecate GUI'
    is_running = False
    is_minimised = False

    is_visible = False
    instance = None
    live_env = hec_gui_startup.pylivecoding.LiveEnvironment

    previous_time = 0
    current_time = 0
    elapsed_time = 0
    debug = False
    _handle = 0
    dev_mode = False
    world = None
    ephestos_component = None
    context = None
    reload_times = 0
    log = None
    pylive = False
    initialising=False
    edit_mode_selection_mode = 'VERT'

    def __init__(self):
    
        self.enable_debug = False
        self._handle = 0
        
        self.previous_context = None
        self.args = None
        self.debug = False
        self.mouse_entered_popup = False
        self.addon_preferences = None
        self.pr=None
        self.ps=None

    @property
    def mouse_over_popup(self):
        apx1 = __class__.world.position[0]-__class__.world.mouse_limit
        apy1 = __class__.world.position[1]-__class__.world.mouse_limit
        apx2 = (__class__.world.position[0] + __class__.world.width)+__class__.world.mouse_limit
        apy2 = (__class__.world.position[1] + __class__.world.height)+__class__.world.mouse_limit
        ex = __class__.world.mouse_position[0]
        ey = __class__.world.mouse_position[1]
        result = ( ex > apx1 and ex < apx2 and ey > apy1 and ey < apy2)
        return result

    def modal(self, context, event):
        self.addon_preferences = context.preferences.addons[__package__].preferences
        __class__.dev_mode = self.addon_preferences.dev_mode
        __class__.pylive = self.addon_preferences.dev_python_live_coding
        __class__.context = context
        
        if __class__.world is None:
            return {'PASS_THROUGH'}
        if self.pr is None and __class__.dev_mode and self.addon_preferences.dev_python_profile:
            self.pr = cProfile.Profile()
            self.pr.enable()
        # handles events (mouse and keyboard)
        # terminate other instances of this class to make sure only one executes at time
        #pdb.set_trace()
        #__class__.world.mouse_position= [event.mouse_region_x + self.context.region.x ,event.mouse_region_y+ self.context.region.y]
        
        if self.mouse_over_popup:
            if not self.mouse_entered_popup:
                #print(f"mouse entered world:[{__class__.world.position,__class__.world.width,__class__.world.height}]")
                self.mouse_entered_popup=True
        if not self.mouse_over_popup:
            if self.mouse_entered_popup:
                #print(f"mouse exited world:[{__class__.world.position,__class__.world.width,__class__.world.height}]")
                self.mouse_entered_popup=False
            if __class__.world.can_hide:
                __class__.is_running = False
        if __class__.instance is not self:
            #print(f"Hecate GUI: terminating (double) instance with id: {id(self)}")
            return {'CANCELLED'}
        # terminate if you suppose to not be running
        if not __class__.is_running and __class__.world.can_hide:
            #print(f"Hecate GUI: operator (not running) finished with id: {id(self)}")
            __class__.world.is_hidden=True
            
            __class__.world.on_event(event=event, context=context)
            
            if __class__.ephestos_component is not None:
                __class__.ephestos_component.unload()
                __class__.ephestos_component = None
                #print(f"Hecate GUI: unloaded Ephestos component , world can hide: {__class__.world.can_hide}, world hide: {__class__.world.is_hidden}")
                if __class__.dev_mode and self.addon_preferences.dev_python_profile:
                    
                    self.pr.disable()
                    s = io.StringIO()
                    sortby = 'cumulative'
                    self.ps = pstats.Stats(self.pr, stream=s).sort_stats(sortby)
                    self.ps.print_stats()
                    #print("==================================================")
                    #print("|         START PROFILE REDRAW                   |")
                    #print("==================================================")
                    #print(f"profile initialisation of world:  {s.getvalue()}")
                    #print("==================================================")
                    #print("|         END PROFILE REDRAW                     |")
                    #print("==================================================")
                    self.pr=None
            return {'CANCELLED'}
        # handle events only inside the viewport
        if context.area is not None and context.area.type == 'VIEW_3D':

            try:
                # live reload module for live coding unless dev mode is turned off
                if __class__.dev_mode and __class__.pylive:               
                    __class__.live_env.update()
                    __class__.reload_times = __class__.reload_times + 1
                   
                    
                if event.type in {'ESC'}:
                    #print(f"Hecate GUI: operator finished with id: {id(self)}")
                    
                    __class__.is_running = False
                
                __class__.world.on_event(event=event, context=context)
                
            except Exception as inst:
                # in case of error trigger the debugger
                type, value, tb = sys.exc_info()
                traceback.print_exc()
                if __class__.dev_mode:
                    pdb.post_mortem(tb)
                    if __class__.pylive:
                        __class__.live_env.update()

           
            # if the event has been used by Hecate do not return it back to Blender
            if __class__.world.consumed_event:
                __class__.world.consumed_event = False
                return {'RUNNING_MODAL'}
        
        #__class__.log_debug.debug(f"pass through event type: {event.type} value: {event.value}")
        return {'PASS_THROUGH'}
    # create the GUI
    def initialise_world(self,context,event):
        if __class__.dev_mode and __class__.pylive:
            __class__.live_env.update()
        if not __class__.initialising:
           
            __class__.initialising = True
            #print("started initialisation")
            cgui = hec_gui_startup.GUI(self,context,event,name="gui")
            bpy.ops.wm.hec_popup_gui('INVOKE_DEFAULT')
            __class__.initialising = False
            #print("ended initialisation")
            return

    @classmethod
    def poll(cls, context):
        return True

    def invoke(self, context, event):
        self.addon_preferences = context.preferences.addons[__package__].preferences
        __class__.dev_mode = self.addon_preferences.dev_mode
        
        __class__.instance = self
        __class__.is_running = True
        # when modal is first triggered
        if __class__.ephestos_component is None:
            __class__.ephestos_component=ephpy.apy.ci.Component(name="hecate_core")
            __class__.ephestos_component.load()
            #print("loaded hecate.acl")
            
        # if another instance existed before you delete it
        if context.area.type == 'VIEW_3D' :
            try:
               
                self.initialise_world(context,event)
                world=__class__.world
                
                #if __class__.dev_mode:
                    #print(f"Hecate GUI: invoke operator with id: {id(__class__.instance)} class id: {id(__class__)} world id: {id(__class__.world)} ww: {__class__.world.width} wh:{__class__.world.height} at mouse:[{event.mouse_region_x}, {event.mouse_region_y}] is_hidden: {__class__.world.is_hidden} children: {__class__.world.children} catalog worlds: {world}")
                

            except Exception as inst:
                type, value, tb = sys.exc_info()
                traceback.print_exc()
                if __class__.dev_mode:
                    pdb.post_mortem(tb)
                    if __class__.pylive:
                        __class__.live_env.update()


            self.args =(context,)
            context.window_manager.modal_handler_add(self)
            self.previous_context = context
            self.__class__.context = context
            return {'RUNNING_MODAL'}
        
        else:
            self.report({'WARNING'}, "View3D not found, cannot open Hecate GUI")
            return {'CANCELLED'}

class HecGUIAddonPreferences(AddonPreferences):
    # this must match the add-on name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __package__

    layout_enable: BoolProperty(
        name="Layout",
        description = "this will override layout map file",
        default=False,
    )
    layout_refresh: BoolProperty(
        name="refresh",
        description = "control reloading layout map files and refreshing",
        default=False,
    )
    layout_refresh_rate: FloatProperty(
        name = "refresh layout rate",
        description = "The rate of refresh in seconds",
        default = 0.1,
        min = 0,
        max = 10

    )
    layout_auto_refresh: BoolProperty(
        name="auto referesh layout",
        description = "automatically refresh layout",
        default=False,
    )
    layout_folders: BoolProperty(
        name = "folders",
        description = "this will override default paths",
        default = False
    )
    layout_filename: StringProperty(
        name = "layout file",
        description = "the relative path of the layout file , including the folder and any subfolders and the full filename",
        default = "right_click_menu/hec_rcmenu_hlm.json",
        subtype = "FILE_PATH"
    )
    layout_icons_folder: StringProperty(
        name = "icons folder",
        description = "folder where icons can be found",
        default = "right_click_menu/hec_rcmenu_hlm.json",
        subtype = "DIR_PATH"
    )
    layout_default_values: BoolProperty(
        name="default values",
        description = "these values will override corresponding variables in the layout file",
        default=False,
    )
    layout_world_width: IntProperty(
        name = "world width",
        description = "world width, the width of the menu",
        min = 0,
        default = 360,
    )
    layout_world_height: IntProperty(
        name = "world height",
        description = "world height, the height of the menu",
        min = 0,
        default = 360,
    )
    layout_world_clipping: BoolProperty(
        name="clipping",
        description = "when this is enabled anything outside the boundaries of world will not be drawn",
        default=False,
    ) 
    layout_morph_width: IntProperty(
        name = "morph width",
        description = "morph width, fefault width for morphs",
        default = 36,
        min = 0,
        max = 200,
    )
    layout_morph_height: IntProperty(
        name = "morph height",
        description = "morph height, default height for morphs",
        default = 36,
        min = 0,
        max = 200,
    )
    layout_world_mouse_margin: IntProperty(
        name = "mouse margin",
        description = "how many pixels away from world area must the mouse be to close the menu automatically",
        default = 12,
        min = 0 ,
        max = 1000
    )

    events_enable: BoolProperty(
        name="Events:",
        description = "override events default behavior",
        default=True,
    )
    events_pass_through: BoolProperty(
        name="pass through",
        description = "should the menu block events",
        default=True,
    )
    events_snap_to_region: BoolProperty(
        name="snap to region",
        description = "should the menu stay inside region",
        default=True,
    )
    
    dev_mode: BoolProperty(
        name="dev mode",
        description = "Warning use this section ONLY if you know what you are doing!",
        default=False,
    )
    dev_debug_mode: BoolProperty(
        name="debug mode",
        description = "debug mode triggers python debugger on error, WARNING: THIS WILL FREEZE BLENDER WAITING FOR INPUT , always enables this if you run Blender from terminal or you have terminal already open so you can interact with the debugger",
        default=False,
    )

    dev_python_live_coding: BoolProperty(
        name="python live coding",
        description = "the GUI will use the pylivecoding library of Ephestos Python API, any change to the python source will auto update the execution while the addon is active. Thus you can use your source code as a real time console testing coding on the fly without the need to restart Blender or the addon. WARNING: This may affect performance",
        default=False,
    )
    dev_python_profile: BoolProperty(
        name="python profile",
        description = "it creates a profile which is then print to the terminal when menu is close, with detailed performance stats.WARNING: This may affect performance",
        default=False,
    )

    def draw(self, context):
        layout = self.layout
        box_main = layout.box()
        
        col_main = box_main.column(align=True)
        col_main.prop(self,"layout_enable")

        box_layout = box_main.box()
        box_layout.enabled = self.layout_enable
        col_layout = box_layout.column(align=True)
        #col_layout.prop(self,"layout_refresh")

        
        box_refresh_layout = col_layout.box()
        box_refresh_layout.enabled = self.layout_refresh
        row_refresh_layout = box_refresh_layout.row(align=True)
        #row_refresh_layout.prop(self, "layout_auto_refresh")
        #row_refresh_layout.prop(self, "layout_refresh_rate")
        
        #col_layout.prop(self,"layout_folders")

        box_folders_layout = col_layout.box()
        box_folders_layout.enabled=self.layout_folders
        col_folders_layout = box_folders_layout.column(align=True)
        #col_folders_layout.prop(self,"layout_icons_folder")
        #col_folders_layout.prop(self, "layout_filename")
        
        col_layout.prop(self,"layout_default_values")

        box_default_values_layout = col_layout.box()
        box_default_values_layout.enabled = self.layout_default_values 
        col_default_values_layout = box_default_values_layout.column(align=True)
        col_default_values_layout.prop(self,"layout_world_mouse_margin") 
        col_default_values_world_layout = box_default_values_layout.column(align=True)
        row_default_values_world_layout = col_default_values_world_layout.row(align=True)
        row_default_values_world_layout.prop(self, "layout_world_width")
        row_default_values_world_layout.prop(self, "layout_world_height")
        #row_default_values_world_layout.prop(self, "layout_world_clipping")
        col_default_values_morph_layout = box_default_values_layout.column(align=True)
        row_default_values_morph_layout = col_default_values_morph_layout.row(align=True)
        row_default_values_morph_layout.prop(self, "layout_morph_width")
        row_default_values_morph_layout.prop(self, "layout_morph_height")
                   
        col1_main = box_main.column(align=True)
        #col1_main.prop(self,"events_enable")
        box_events = box_main.box()
        box_events.enabled = self.events_enable
        row_events = box_events.row(align=True)
        #row_events.prop(self, "events_pass_through")
        #row_events.prop(self, "events_snap_to_region")
        
        col2_main = box_main.column(align=True)
        row_main = col2_main.row(align=True)
        row_main.label(icon="ERROR")
        row_main.prop(self,"dev_mode")
        box_dev = box_main.box()
        box_dev.enabled = self.dev_mode
        row_dev = box_dev.row(align=True)
        row_dev.prop(self, "dev_debug_mode")
        row_dev.prop(self, "dev_python_live_coding")
        row_dev.prop(self, "dev_python_profile")
        
        



def register():
    """EPHESTOS_OT_hec_gui.log_debug.debug("Hecate GUI: registring")"""
    
    bpy.utils.register_class(EPHESTOS_OT_hec_gui)
    bpy.utils.register_class(HecGUIAddonPreferences)
    """EPHESTOS_OT_hec_gui.ephestos_component=ephpy.apy.ci.Component(name="hecate_core")
    EPHESTOS_OT_hec_gui.ephestos_component.load()
    EPHESTOS_OT_hec_gui.log_debug.debug("Hecate GUI: loaded Ephestos component")"""
   
def unregister():
    bpy.utils.unregister_class(EPHESTOS_OT_hec_gui)
    bpy.utils.unregister_class(HecGUIAddonPreferences)
    """if EPHESTOS_OT_hec_gui.ephestos_component is not None:
        EPHESTOS_OT_hec_gui.ephestos_component.unload()
        EPHESTOS_OT_hec_gui.log_debug.debug("Hecate GUI: unloaded Ephestos component")
        EPHESTOS_OT_hec_gui.ephestos_component = None"""
    

if __name__ == "__main__":

    register()








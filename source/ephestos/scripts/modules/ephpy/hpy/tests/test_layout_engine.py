import unittest, os
from ..layout_engine import *
from .. import core
from hec_gui import morph_actions


class TestLayoutEngine(unittest.TestCase):
    def setUp(self):
        self.layout_engine = None
        self.g_world = None
        self.sample_json_dict = None
        self.world_dict = {
            "name": "world",
            "type": "world",
            "parent": "none",
            "width": 360,
            "height": 360,
            "mouse_limit": 20,
            "scale": 1,
        }

        self.background_dict = {
            "name": "background",
            "type": "morph",
            "parent": "world",
            "icon": "background",
            "x": 10,
            "y": 10,
            "width": 360,
            "height": 300,
        }

        self.morph_parent_dict = {
            "name": "object_tab_container",
            "parent": "world",
            "type": "container",
            "x": 0,
            "y": 0,
        }

        self.morph_child_dict = {
            "name": "object_tab_button",
            "parent": "object_tab_container",
            "type": "switch",
            "on_left_click_action": "execute:print('hello world')",
            "info": "Object Mode",
            "icon": "object_mode",
            "main_morph": True,
            "x": 0,
            "y": 0,
        }

        self.json_content = {
            "morphs": [
                self.world_dict,
                self.background_dict,
                self.morph_parent_dict,
                self.morph_child_dict,
            ]
        }

    def initialise_layout_engine(self):
        self.layout_engine = LayoutEngine()

    def set_morph_actions(self):
        self.layout_engine.morph_actions = morph_actions

    def load_json_file(self):
        self.layout_engine.load_default_layout_file(
            "right_click_menu/hec_rcmenu_hlm.json"
        )

    def generate_world(self):
        self.g_world = self.layout_engine.generate_morphs()

    def standard_initialisation(self):
        self.initialise_layout_engine()
        self.set_morph_actions()
        self.load_json_file()
        self.generate_world()

    def test_layout_engine_initialisation(self):
        self.initialise_layout_engine()
        self.assertIsInstance(self.layout_engine, LayoutEngine)

    def test_json_loading(self):
        self.initialise_layout_engine()
        self.set_morph_actions()
        self.load_json_file()
        self.assertIsNotNone(self.layout_engine.json_content)
        self.assertIsInstance(self.layout_engine.json_content, dict)

    def test_initialise_world_state_from_json_dict(self):
        self.initialise_layout_engine()
        gworld = self.layout_engine.generated_world
        self.layout_engine.initialise_world_state_from_json_dict(
            gworld, self.world_dict
        )
        self.assertEqual(gworld.width, self.world_dict["width"])
        self.assertEqual(gworld.height, self.world_dict["height"])
        self.assertEqual(gworld.mouse_limit, self.world_dict["mouse_limit"])
        self.assertTrue(gworld.children == [])

    def test_parse_world_child_from_json_dict(self):
        self.initialise_layout_engine()
        gworld = self.layout_engine.generated_world

        self.layout_engine.initialise_world_state_from_json_dict(
            gworld, self.world_dict
        )
        self.assertEqual(gworld.children, [])
        self.assertEqual(len(gworld.children), 0)

        self.layout_engine.parse_world_child_from_json_dict(
            gworld, self.background_dict
        )
        self.assertEqual(len(gworld.children), 1, msg=f"children: {gworld.children}")

        self.assertIs(type(gworld.children[0]), core.Morph)
        self.assertEqual(gworld.children[0].name, self.background_dict["name"])
        self.assertEqual(
            gworld.children[0].active_texture.layers[0],
            self.background_dict["icon"] + ".png",
        )
        self.assertEqual(
            gworld.children[0].position,
            [self.background_dict["x"], self.background_dict["y"]],
        )
        self.assertEqual(gworld.children[0].width, self.background_dict["width"])
        self.assertEqual(gworld.children[0].height, self.background_dict["height"])

    def test_parse_morph_child_from_json_dict(self):
        self.initialise_layout_engine()
        gworld = self.layout_engine.generated_world

        self.layout_engine.initialise_world_state_from_json_dict(
            gworld, self.world_dict
        )
        self.assertEqual(gworld.children, [])
        self.assertEqual(len(gworld.children), 0)

        self.layout_engine.parse_world_child_from_json_dict(
            gworld, self.morph_parent_dict
        )
        self.assertEqual(len(gworld.children), 1, msg=f"children: {gworld.children}")

        self.assertIs(type(gworld.children[0]), core.Morph)
        self.assertEqual(gworld.children[0].name, self.morph_parent_dict["name"])
        self.assertTrue(gworld.children[0].is_container)
        self.assertEqual(
            gworld.children[0].position,
            [self.morph_parent_dict["x"], self.morph_parent_dict["y"]],
        )

        self.layout_engine.parse_morph_child_from_json_dict(
            gworld, self.morph_child_dict
        )
        self.assertEqual(len(gworld.children[0].children), 1)
        self.assertIsInstance(gworld.children[0].children[0], core.SwitchButtonMorph)
        self.assertEqual(
            gworld.children[0].children[0].name, self.morph_child_dict["name"]
        )
        # self.assertEqual(gworld.children[0].height,self.morph_parent_dict["height"])

    def test_parse_action(self):
        self.initialise_layout_engine()
        self.set_morph_actions()
        self.load_json_file()
        morph_entry = "something"
        json_morph_dict = {
            "name": "some_morph",
            "type": "morph",
            "parent": "parent_morph",
            "icon": "some_icon",
            "on_left_click_action": "execute:morph = 'hello world';return morph",
        }
        action = "on_left_click_action"
        morph_common_dict = {"name": "some_common_morph"}

        def func():
            pass

        result = self.layout_engine.parse_action(
            morph_common_dict, action, json_morph_dict
        )
        self.assertEqual(result["name"], "some_common_morph")
        self.assertIs(type(result["on_left_click_action"]), type(func))
        morph = result[action](morph_entry)
        self.assertEqual(morph, "hello world")

    def test_generate_morphs(self):
        self.initialise_layout_engine()
        self.layout_engine.json_content = self.json_content
        gworld = self.layout_engine.generated_world

        self.layout_engine.generate_morphs()

        self.assertEqual(len(gworld.children), 2, msg=f"children: {gworld.children}")
        self.assertEqual(gworld.children[0].name, self.background_dict["name"])


"""/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */
"""
from .test_core import *
import importlib
import sys
modules=['ephpy.hpy.tests','ephpy.hpy.tests.test_morph','ephpy.hpy.tests.test_layout_engine','ephpy.hpy','ephpy.hpy.layout_engine']
def run():
    for x in range(0,len(modules)):
        mod = modules[x]
        importlib.reload(sys.modules[mod])
    ht = HecateTester()
    ht.run()
    print("Ephestos total tests...")
    print("start testing")
    print("-------------")
    print("test result : ",ht.test_result)

    for f in ht.test_result.failures:
        print("failed test name : ",f[0].id())
        print("test failure : \n",f[1])

    if ht.test_result.errors != []:
        for e in ht.test_result.errors:
            print("failed test name : ",e[0].id())
            print("test error : \n",e[1])
    print("number of tests run: {}".format(ht.test_result.testsRun))   
    print("finished testing")
    print("****************")
    return ht

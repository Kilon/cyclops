import unittest
from ephpy.hpy import core
import hec_gui


class TestMorph(unittest.TestCase):
    def setUp(self):
        self.morph1 = core.Morph()
        self.morph2 = core.Morph()
        self.world = core.World()
        self.morph1.width = 36
        self.morph1.height = 36
        self.morph1.position = [0, 0]
        self.morph2.width = 36
        self.morph2.height = 36
        self.morph2.position = [0, 0]

    def test_creation(self):
        morph = core.SwitchButtonMorph()
        morph.world = core.World()
        self.assertIsInstance(morph, core.Morph)
        self.assertEqual(morph.position, [0, 0])
        self.assertIsNotNone(morph.world)

    def test_parenting(self):
        morph = core.Morph()
        morph.world = core.World()
        child = core.Morph()
        morph.add_morph(child)
        self.assertIs(child, morph.children[0])
        self.assertIs(morph, child.parent)
        self.assertIsNotNone(morph.world)
        self.assertIsNotNone(child.world)

    def test_position(self):
        self.world.add_morph(self.morph1)
        self.morph1.add_morph(self.morph2)
        self.morph1.position = [30, 30]
        self.morph2.position = [70, 70]
        self.assertEqual(self.morph2.position, [70, 70])
        self.assertEqual(self.morph2.world_position, [100, 100])
        self.assertEqual(self.morph2.absolute_position, [100, 100])
        self.assertEqual(self.morph1.world_position, [30, 30])

    def test_point_inside_area(self):
        self.world.add_morph(self.morph1)
        is_inside = self.morph1.point_inside_area([30, 30], [0, 0, 500, 500])
        self.assertTrue(is_inside)

    def test_is_overlaping_morph(self):
        self.world.add_morph(self.morph1)
        self.morph2.position = [10, 10]
        self.assertTrue(self.morph1.is_overlapping_morph(self.morph2))

    def test_is_inside_morph(self):
        self.world.add_morph(self.morph1)
        self.morph2.position = [30, 30]
        self.morph1.width = 200
        self.morph1.height = 200
        self.assertTrue(self.morph1.is_inside_morph(self.morph1))

    def test_autoresize_width(self):

        self.world.add_morph(self.morph1)
        self.morph1.add_morph(self.morph2)
        self.assertNotEqual(self.morph1.width, 200)
        self.morph2.width = 200
        self.assertNotEqual(self.morph1.width, 200)
        self.morph1.can_autoresize = True
        self.morph2.width = 200
        self.assertEqual(self.morph1.width, 200)

    def test_autoresize_height(self):
        self.world.add_morph(self.morph1)
        self.morph1.add_morph(self.morph2)
        self.assertNotEqual(self.morph1.width, 200)
        self.morph2.height = 200
        self.assertNotEqual(self.morph1.height, 200)
        self.morph1.can_autoresize = True
        self.morph2.height = 200
        self.assertEqual(self.morph1.height, 200)

    def test_autoresize_position(self):
        self.world.add_morph(self.morph1)
        self.morph1.add_morph(self.morph2)
        self.morph2.position = [34, 0]
        self.assertNotEqual(self.morph1.width, 70)
        self.assertEqual(self.morph1.height, 36)
        self.morph1.can_autoresize = True
        self.morph2.position = [34, 0]
        self.assertEqual(self.morph1.width, 70)


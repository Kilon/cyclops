#ifndef __EPH__HECATE_ICON_MENU_H__
#define __EPH__HECATE_ICON_MENU_H__

#include <stdio.h>



#include "IMB_imbuf.h"
#include "UI_interface.h"
#include "BLF_api.h"
#include "BLF_api.h"
#include "BKE_context.h"
#include "ED_screen.h"
#include "DNA_scene_types.h"
#include "DNA_windowmanager_types.h"
#include "BLI_utildefines.h"
#include "BKE_appdir.h"
#include "ephestos.h"

void WM_OT_hec_popup_gui(wmOperatorType *ot);
uiBlock *eph_wm_block_popup_icons_menu(bContext *C, ARegion *ar, void *userdata);
#endif /* __EPH_HECATE_ICON_MENU_H__ */

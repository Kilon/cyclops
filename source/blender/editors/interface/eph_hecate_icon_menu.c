/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */

#include "eph_hecate_icon_menu.h"


/* ***************** Hecate GUI  ************************* */

struct SearchPopupInit_Data {
	int size[2];
};
WorldMorph *hec_world = NULL;
aci_registry aci_library[]={{NULL,NULL},{NULL,NULL},{NULL,NULL}};
struct aci_imported_functions aci_ef;
aci_request_def aci_request_if=NULL;

WorldMorph *eph_hecate_get_world()
{
	if(hec_world)
	{
		return hec_world;
	}
	else
	{
		hec_world = (WorldMorph*)malloc(sizeof(WorldMorph));
		hec_world->region = NULL;
		hec_world->block = NULL;
		hec_world->first_texture = NULL;
		hec_world->last_texture = NULL; 
		hec_world->first_morph = NULL;
		hec_world->last_morph = NULL;
		hec_world->x = 0;
		hec_world->y = 0;
		hec_world->width = 0;
		hec_world->height = 0;
		hec_world->py_region_x = 0;
		hec_world->py_region_y = 0;
		hec_world->py_region_width = 0;
		hec_world->py_region_height = 0;
		hec_world->py_mouse_x = 0;
		hec_world->py_mouse_y = 0;
		hec_world->logb = NULL;
		hec_world->should_refresh = false;
		hec_world->offset_x = 0;
		hec_world->offset_y = 0;
		hec_world->is_hidden = 0;
		return hec_world; 
	}
	
};
void* eph_malloc(size_t mem_size)
{
	return malloc(mem_size);
};

void eph_free(void* mpointer)
{
	free(mpointer);
};
/* Ephestos main functions exported to the components*/
static void blender_create_functions_table(struct aci_imported_functions *function_struct)
{
	function_struct->bl_BKE_appdir_folder_id = BKE_appdir_folder_id;
	function_struct->bl_snprintf = snprintf;
	function_struct->bl_IMB_loadiffname = IMB_loadiffname;
	function_struct->bl_uiDefIconButO = uiDefIconButO;
	function_struct->bl_uiDefBut = uiDefBut;
	function_struct->bl_BLF_batch_draw_flush = BLF_batch_draw_flush;
	function_struct->bl_BLF_size = BLF_size;
	function_struct->bl_BLF_default = BLF_default;
	function_struct->bl_BLF_position = BLF_position;
	function_struct->bl_BLF_draw = BLF_draw;
	function_struct->bl_UI_but_flag_enable=UI_but_flag_enable;
	function_struct->bl_uiDefIconBut = uiDefIconBut;
	function_struct->bl_UI_block_bounds_set_popup = UI_block_bounds_set_popup;
	function_struct->bl_IMB_dupImBuf = IMB_dupImBuf;
	function_struct->ED_region_tag_redraw = ED_region_tag_redraw;
	function_struct->eph_hecate_get_world = eph_hecate_get_world;
	function_struct->eph_malloc = eph_malloc;
	function_struct->eph_free = eph_free;
	function_struct->bl_UI_block_translate = UI_block_translate;
};

DynamicLibrary* aci_load(char *component_name, char *relative_path)
{
	DynamicLibrary *lib;
	int aci_library_size = (int)(sizeof(aci_library)/sizeof(aci_registry));
	for(int x=0; x<aci_library_size;x++)
	{
		if(aci_library[x].name == component_name)
		{
			return aci_library[x].component;
		}
	}
	const char *folder = BKE_appdir_folder_id(BLENDER_SYSTEM_DATAFILES, "ephestos/acl");
	char dll_filepath[256];
	snprintf(dll_filepath, sizeof dll_filepath, "%s%s%s%s", folder, relative_path,component_name,".acl");
	lib = BLI_dynlib_open(dll_filepath);
	aci_library[0].component = lib;
	aci_library[0].name = component_name;
	aci_request_if = BLI_dynlib_find_symbol(lib, "aci_request");

	blender_create_functions_table(&aci_ef);
	export_functions_table = BLI_dynlib_find_symbol(lib, "aci_import_functions_table");
	export_functions_table(&aci_ef);

	return lib;
};


DynamicLibrary* aci_unload(char *component_name)
{
	int aci_library_size = (int)(sizeof(aci_library)/sizeof(aci_registry));
	for(int x=0; x<aci_library_size;x++)
	{
		if(aci_library[x].name == component_name)
		{
			BLI_dynlib_close(aci_library[x].component);
			aci_library[x].component = NULL;
			aci_library[x].name = NULL;
			aci_request_if = NULL;
			return aci_library[x].component;
		}
	}
	return NULL;
};

uiBlock *eph_wm_block_popup_icons_menu(bContext *C, ARegion *ar, void *userdata)
{
	hec_world->region = ar; 
	hec_world->block = UI_block_begin(C, ar, "__hec_gui_popup", UI_EMBOSS);
	UI_block_flag_disable(hec_world->block,UI_BLOCK_LOOP);	
	UI_block_theme_style_set(hec_world->block, UI_BLOCK_THEME_STYLE_POPUP);
	aci_arg args[]={{.type="block_pointer",.block_pointer = hec_world->block},{.type="region",.region=ar}};
	char hec_init_error[80];
	if(aci_request_if)
	{
		aci_request_if("hec_buttons_init",2,args,hec_init_error);
	}
	else
	{
		UI_block_flag_enable(hec_world->block, UI_BLOCK_MOVEMOUSE_QUIT | UI_BLOCK_LOOP | UI_BLOCK_KEEP_OPEN |  UI_BLOCK_NO_WIN_CLIP );
		UI_block_bounds_set_popup(hec_world->block, 0, (const int[2]){0, 0});
		uiDefBut(hec_world->block,UI_BTYPE_IMAGE, 0, "", 2000, 2000, 12,12, NULL,0,0,0,0, "");
		UI_block_translate(hec_world->block,2000,2000);
		//hec_world->block->active = false;
		//WM_event_add_mousemove(C);
		//ar->visible = false;
	}
	
	
	return hec_world->block;
	
};

static int eph_wm_popup_icons_menu_exec(bContext *UNUSED(C), wmOperator *UNUSED(op))
{
	
	return OPERATOR_FINISHED;
};

static int eph_wm_search_menu_invoke(bContext *C, wmOperator *op, const wmEvent *event)
{

	//ED_workspace_status_text(C, "LMB: Ephestos is running");
	UI_popup_block_invoke(C, eph_wm_block_popup_icons_menu, op, NULL);
	//return OPERATOR_FINISHED; 
 	//WM_event_add_modal_handler(C, op);
	return OPERATOR_INTERFACE ;
};

static int eph_popup_menu_modal(bContext *C, wmOperator *op, const wmEvent *event)
{
	char *error_message[80];
	aci_arg args[]={{.type="bContext",.context = C},{.type="wmOperator",.operator=op},{.type="wmEvent",.event=event}};
	//aci_request_if("onEvent",3,args,error_message);
	if(hec_world->block->active == 0)
	{
		//printf("operator finished\n");
		//Ephestos_exit();
		return OPERATOR_FINISHED;
	}

	if(event->type == LEFTMOUSE && event->val == KM_PRESS)
	{
		printf("left mouse click\n");
	}

	return OPERATOR_PASS_THROUGH ;
}


/* op->poll */
static bool eph_wm_popup_icons_menu_poll(bContext *C)
{
	if (CTX_wm_window(C) == NULL) {
		return 0;
	}
	else {
		ScrArea *sa = CTX_wm_area(C);
		if (sa) {
			if (sa->spacetype == SPACE_CONSOLE) return 0;  /* XXX - so we can use the shortcut in the console */
			if (sa->spacetype == SPACE_TEXT) return 0;     /* XXX - so we can use the spacebar in the text editor */
			if (sa->spacetype != SPACE_VIEW3D) return 0;
		}
		else {
			Object *editob = CTX_data_edit_object(C);
			if (editob && editob->type == OB_FONT) return 0;  /* XXX - so we can use the spacebar for entering text */
		}
	}
	return 1;
}

/* Hecate GUI operator unlike python equivelant this one only handles drawing and not events */
void WM_OT_hec_popup_gui(wmOperatorType *ot)
{
	ot->name = "Ephestos Search Menu";
	ot->idname = "WM_OT_hec_popup_gui";
	ot->description = "Pop-up a search menu over all available operators in current context";

	ot->invoke = eph_wm_search_menu_invoke;
	//ot->exec = eph_wm_popup_icons_menu_exec;
	ot->poll = eph_wm_popup_icons_menu_poll;
	//ot->modal = eph_popup_menu_modal;
}

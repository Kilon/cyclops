@echo off


set doc_type=%2
set kilon_gitlab_dir_from_blender_dir=../kilon.gitlab.io/
set kilon_blender_dir_from_gitlab_dir=../blender
set ephestos_source_dir=./source/ephestos
set ephestos_doc_dir=%ephestos_source_dir%/doc
set blender_version=2.90
set ephestos_components_build_dir=../ephestos_component_libraries
set ephestos_release_build_dir=../build_windows_Release_x64_vc16_Release/bin/Release
set ephestos_debug_build_dir=../build_windows_x64_vc16_Debug/bin/Debug
set ephestos_scripts_source_dir=%ephestos_source_dir%/scripts
set ephestos_release_build_datafiles_dir=%ephestos_release_build_dir%/%blender_version%/datafiles
set ephestos_debug_build_datafiles_dir=%ephestos_debug_build_dir%/%blender_version%/datafiles
set ephestos_release_build_scripts_dir=%ephestos_release_build_dir%/%blender_version%/scripts
set ephestos_debug_build_scripts_dir=%ephestos_debug_build_dir%/%blender_version%/scripts
set ephestos_debug_build_doc_dir = %ephestos_debug_build_dir%/docs
set ephestos_release_build_doc_dir=%ephestos_release_build_dir%/docs

IF "%1" == "-br" CALL :build_release
IF "%1" == "-bd" CALL :build_debug
IF "%1" == "-bb" CALL :both
IF "%1" == "-cad" CALL :addons_debug
IF "%1" == "-cad-ldt" CALL :addons_debug_launch_tests
IF "%1" == "-car" CALL :addons
IF "%1" == "-bcr" CALL :components
IF "%1" == "-bcd" CALL :components_debug
IF "%1" == "-h" CALL :help
IF "%1" == "-lr" CALL :launch
IF "%1" == "-ld" CALL :launch_debug
IF "%1" == "-ldt" CALL :launch_debug_tests
IF "%1" == "-tr" CALL :launch_test_release
IF "%1" == "-td" CALL :launch_test_debug
IF "%1" == "-log" CALL :git_log
IF "%1" == "-bdd" CALL :doc_build_debug %doc_type%
IF "%1" == "-bdr" CALL :doc_build_release %doc_type%
IF "%1" == "-kilon" CALL :kilon %2 %3 %4 %5 %6 %7 %8 %9
IF "%1" == "-k" CALL :kilon %2 %3 %4 %5 %6 %7 %8 %9
if "%1" == "" CALL :help

@EXIT /B %ERRORLEVEL%

:git_log
    git log ^--author=kilon ^-^-no-merges ^-n 30  ^-^-pretty="%%Cgreen %%ad %%Cred %%an %%C(cyan) %%s" 
    EXIT /B 0 

:ephestos_banner
    echo +----------------------------------------------------------------------------------+                                                                 
    echo ^|  ______   ______   ___   ___   ______   ______   _________  ______   ______      ^|      
    echo ^| /_____/\ /_____/\ /__/\ /__/\ /_____/\ /_____/\ /________/\/_____/\ /_____/\     ^|   
    echo ^| \::::_\/_\:::_ \ \\::\ \\  \ \\::::_\/_\::::_\/_\__.::.__\/\:::_ \ \\::::_\/_    ^|  
    echo ^|  \:\/___/\\:(_) \ \\::\/_\ .\ \\:\/___/\\:\/___/\  \::\ \   \:\ \ \ \\:\/___/\   ^|  
    echo ^|   \::___\/_\: ___\/ \:: ___::\ \\::___\/_\_::._\:\  \::\ \   \:\ \ \ \\_::._\:\  ^| 
    echo ^|    \:\____/\\ \ \    \: \ \\::\ \\:\____/\ /____\:\  \::\ \   \:\_\ \ \ /____\:\ ^|
    echo ^|     \_____\/ \_\/     \__\/ \::\/ \_____\/ \_____\/   \__\/    \_____\/ \_____\/ ^|
    echo +----------------------------------------------------------------------------------+
    EXIT /B 0

:build_release
    echo.
    echo.
    echo.
    echo -------------------------------------------------------------
	echo ^| Building Ephestos release build...                        ^|
	echo -------------------------------------------------------------
    echo.
    echo.
    echo.
    cmd /c "make release"
    CALL :components
    CALL :addons
    echo.
    echo -------------------------------------------------------------
	echo ^| finished building Ephestos release build...               ^|
	echo -------------------------------------------------------------
    EXIT /B 0

:build_debug
    echo.
    echo.
    echo.
    echo -------------------------------------------------------------
	echo ^| Building Ephestos debug build...                          ^|
	echo -------------------------------------------------------------
    echo.
    echo.
    echo.
    cmd /c "make debug"
    CALL :components_debug
    CALL :addons_debug
    echo.
    echo -------------------------------------------------------------
	echo ^| Finished building Ephestos debug build...                 ^|
	echo -------------------------------------------------------------
    EXIT /B 0

:both
    call :ephestos_banner
    CALL :build_debug
    CALL :build_release
    EXIT /B 0

:components
    echo.
	echo -------------------------------------------------------------
	echo ^| Building Ephestos components release mode...              ^|
	echo -------------------------------------------------------------
    cmake -G "Visual Studio 16 2019"  -A x64 -S %ephestos_source_dir% -B%ephestos_components_build_dir% 
    cmake  --build  %ephestos_components_build_dir% --clean-first --config Release
    cmake -E copy %ephestos_components_build_dir%/Release/hecate_core.dll %ephestos_release_build_datafiles_dir%/ephestos/acl/core/hecate_core.acl
    echo finished building components!
    EXIT /B 0

:components_debug
    echo.
	echo -------------------------------------------------------------
	echo ^| Building Ephestos components debug   mode...              ^|
	echo -------------------------------------------------------------
    cmake -G "Visual Studio 16 2019"  -A x64 -S %ephestos_source_dir%/acl/hecate/hecate_core -B%ephestos_components_build_dir% 
    cmake  --build  %ephestos_components_build_dir% --clean-first --config Debug
    cmake -E copy %ephestos_components_build_dir%/Debug/hecate_core.dll %ephestos_debug_build_datafiles_dir%/ephestos/acl/core/hecate_core.acl
    echo finished building debug components!
    EXIT /B 0

:addons_debug
    echo.
	echo -------------------------------------------------------------
	echo ^| Copying ephestos addons to debug build ^dir                ^|
	echo -------------------------------------------------------------
    cmake -E copy_directory %ephestos_scripts_source_dir% %ephestos_debug_build_scripts_dir%
    cmake -E copy_directory ./release/datafiles/ephestos %ephestos_debug_build_datafiles_dir%/ephestos
    echo finished copying debug addons!
    EXIT /B 0

:addons
    echo.
	echo -------------------------------------------------------------
	echo ^| Copying ephestos addons to release build ^dir              ^|
	echo -------------------------------------------------------------
    cmake -E copy_directory %ephestos_scripts_source_dir% %ephestos_release_build_scripts_dir%
    cmake -E copy_directory ./release/datafiles/ephestos %ephestos_release_build_datafiles_dir%/ephestos
    echo finished copying addons!
    EXIT /B 0

:doc_build_debug
    echo.
	echo -------------------------------------------------------------
	echo ^| Building and copying doc to debug build ^dir              ^|
	echo -------------------------------------------------------------
    call  %ephestos_doc_dir%/make.bat %1
    echo finished building docs
    cmake -E copy_directory %ephestos_doc_dir%/_build/%1 %ephestos_debug_build_doc_dir%/
    echo finished copying docs to debug build dir
    EXIT /B 0

:doc_build_release
    echo.
	echo -------------------------------------------------------------
	echo ^| Building and copying doc to release build ^dir            ^|
	echo -------------------------------------------------------------
    call  %ephestos_doc_dir%/make.bat %1
    echo finished building docs
    cmake -E copy_directory %ephestos_doc_dir%/_build/%1 %ephestos_release_build_doc_dir%/
    echo finished copying docs to release build dir
    EXIT /B 0

:kilon
    IF "%1" == "" CALL :help_kilon
    IF "%1" == "-b" CALL :kilon_b
    IF "%1" == "-bc" CALL :kilon_bc %2
    IF "%1" == "-bcp" CALL :kilon_bcp %2
    IF "%1" == "-c" CALL :kilon_c %2
    IF "%1" == "-p" CALL :kilon_p 
    IF "%1" == "-pu" CALL :kilon_pu
    IF "%1" == "-g" CALL :kilon_g 
    IF "%1" == "-e" CALL :kilon_e %2 %3 %4 %5 %6 %7 %8  
    EXIT /B 0

:kilon_b:
    echo.
    echo -------------------------------------------------------------
	echo ^|  buidling and copying docs to kilon gitlab io ^dir       ^|
	echo -------------------------------------------------------------
    call  %ephestos_doc_dir%/make.bat html
    cmake -E copy_directory %ephestos_doc_dir%/_build/html %kilon_gitlab_dir_from_blender_dir%/ephestos_doc
    echo finished copying 
    EXIT /B 0

:kilon_bc
    echo.
    echo -------------------------------------------------------------
	echo ^| git commit     kilon gitlab io          ^dir            ^|
	echo -------------------------------------------------------------
    CALL :kilon_e git commit -am %1
    EXIT /B 0

:kilon_bcp
    CALL :kilon_b
    CALL :kilon_bc %1
    CALL :kilon_p
    EXIT /B 0

:kilon_e
    CALL :kilon_enter_gitlab_dir
    echo %1 %2 %3 %4 %5 %6 %7 
    %1 %2 %3 %4 %5 %6 %7
    CALL :kilon_enter_blender_dir
    EXIT /B 0

:kilon_p
    CALL :kilon_e git push
    EXIT /B 0

:kilon_pu
    CALL :kilon_e git pull
    EXIT /B 0

:kilon_enter_gitlab_dir
    cd %kilon_gitlab_dir_from_blender_dir%
    EXIT /B 0

:kilon_enter_blender_dir
    cd %kilon_blender_dir_from_gitlab_dir%
    EXIT /B 0


:help
  
    call :ephestos_banner                                                         
    echo ^| syntax: ephestos [option]          Available options :                           ^|
    echo ^+----------------------------------------------------------------------------------^+
	echo ^|  * -br         ^| build the entire release build of Ephestos                      ^|
	echo ^|  * -bd         ^| build the entire debug build of Ephestos                        ^|
	echo ^|  * -bb         ^| build both debug and release build of Ephestos                  ^|
    echo ^|  * -bcr        ^| build only the release components                               ^|
	echo ^|  * -bcd        ^| build only the debug components                                 ^|
	echo ^|  * -car        ^| ^copy ephestos addons to build ^dir                               ^|
	echo ^|  * -cad        ^| ^copy ephestos addons to debug build ^dir                         ^|
    echo ^|  * -cad-ldt    ^| combines -cad and -ldt options                                  ^|
    echo ^|  * -td         ^| combines -cad and -ldt options                                  ^|
    echo ^|  * -tr         ^|  uses -car and loads test ^for Blender release build             ^|
	echo ^|  * -lr         ^| ^start ephestos executable                                       ^|
	echo ^|  * -ld         ^| ^start ephestos debug executable                                 ^|
    echo ^|  * -ldt        ^| ^start ephestos debug executable and launch tests in background  ^|
    echo ^|  * -h          ^| display this help info                                          ^|
    echo ^|  * -log        ^| display pretty formated git log                                 ^|
    echo ^+----------------------------------------------------------------------------------^+
    echo ^|                              DOCUMENTATION                                       ^|
    echo ^+----------------------------------------------------------------------------------^+
    echo ^|  * -bdd ^<type^> ^| build doc and ^copy to debug build ^dir                           ^|
    echo ^|  * -bdr ^<type^> ^| build doc and ^copy to debug release ^dir                         ^|
    echo ^|         ^<type^> ^| to view available options pass without an argument              ^|
    echo ^+----------------------------------------------------------------------------------^+


    EXIT /B 0

:help_kilon
  
    call :ephestos_banner 
    echo ^| WARNING !!!! THESE OPTIONS ARE SECRET MEANT TO BE RUN ONLY BY ME .... ;)         ^|
    echo ^| they need a kilon.gitlab.io ^dir to work                                          ^|
    echo ^| syntax: ephestos -kilon [option]          Available options :                    ^|
    echo ^+----------------------------------------------------------------------------------^+
	echo ^|  * -b          ^| simply ^copy docs to ^dir                                         ^|
	echo ^|  * -bc [msg]   ^| ^copy and git commit with commit message [msg]                   ^|
	echo ^|  * -bcp [msg]  ^| ^copy , git commit with message [msg] and finally git push       ^|
    echo ^|  * -c [msg]    ^| just git commit with message [msg]                              ^|
	echo ^|  * -p          ^| only git push                                                   ^|
	echo ^|  * -pu         ^| only git pull                                                   ^|
    echo ^|  * -g          ^| only run a git command                                          ^|
    echo ^|  * -e [cmds]   ^| only execute bash command or commands    [cmds]                 ^|
    echo ^+----------------------------------------------------------------------------------^+
    
    EXIT /B 0

:launch
    cd  %ephestos_release_build_dir%/ 
    blender
    cd ../../../blender
    EXIT /B 0

:launch_debug
    cd %ephestos_debug_build_dir%/ 
    blender
    cd ../../../blender
    EXIT /B 0

:launch_debug_tests
    cd %ephestos_debug_build_dir%/ 
    blender -b --python-expr "from ephpy.hpy.tests import *; run()"
    cd ../../../blender
    EXIT /B 0

:addons_debug_launch_tests
    CALL :addons_debug
    CALL :launch_debug_tests
    EXIT /B 0

:launch_test_debug
    CALL :addons_debug_launch_tests
    EXIT /B 0

:launch_test_release
    CALL :addons
    cd %ephestos_release_build_dir%/ 
    blender -b --python-expr "from ephpy.hpy.tests import *; run()"
    cd ../../../blender
    EXIT /B 0


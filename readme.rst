
.. Keep this document short & concise,
   linking to external resources instead of including content in-line.
   See 'release/text/readme.html' for the end user read-me.

Ephestos
=======
Ephestos is a 3d graphics application that modifies and extends the source code of Blender to make it act as a container for 6 projects:

1. **Aura**: A plugin architecture that prioritises, performance, modularity and compatibility with Blender
2. **Hecata**: A GUI API for custom look GUIs
3. **Morpheas**: A GUI Designer for Hecate
4. **Cyclops**: A modular internal documentation system for Blender and Ephestos
5. **Nyx**: A asset manager and procedural generator
6. **Talos**: A visual coding programming language acting as visual representation for Blender and Ephestos Python APIs

Blender
=======

Blender is the free and open source 3D creation suite.
It supports the entirety of the 3D pipeline-modeling, rigging, animation, simulation, rendering, compositing,
motion tracking and video editing.

.. figure:: https://code.blender.org/wp-content/uploads/2018/12/springrg.jpg
   :scale: 50 %
   :align: center


Project Pages
-------------

- `Main Website <http://www.blender.org>`__
- `Reference Manual <https://docs.blender.org/manual/en/latest/index.html>`__
- `User Community <https://www.blender.org/community/>`__

Development
-----------

- `Build Instructions <https://wiki.blender.org/wiki/Building_Blender>`__
- `Code Review & Bug Tracker <https://developer.blender.org>`__
- `Developer Forum <https://devtalk.blender.org>`__
- `Developer Documentation <https://wiki.blender.org>`__


License
-------

Both Ephestos and Blender as a whole are licensed under the GNU Public License, Version 3.
Individual files may have a different, but compatible license.

See `blender.org/about/license <https://www.blender.org/about/license>`__ for details.
